package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import openfl.Assets;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	
	private var dragon:PlayerDragon;
	private var enemySwarm:Array<Enemy>;
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void{
		super.create();
		
		dragon = new PlayerDragon(this);
		add(dragon);
		dragon.x = (FlxG.width - dragon.width) / 2;
		dragon.y = FlxG.height - (dragon.height * 2);
		
		enemySwarm = new Array<Enemy>();
		spawnEnemies(6);
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void{
		super.update();
		for (i in 0...enemySwarm.length){
			FlxG.overlap(dragon, enemySwarm[i], onEnemyCollision);
		}
		
		if (enemySwarm.length == 0) {
			spawnEnemies(6);
		}
	}
	
	public function getEnemies():Array<Enemy> {
		return enemySwarm;
	}
	
	private function onEnemyCollision(dragon:PlayerDragon, enemy:Enemy) {
		if (enemy.exists && dragon.exists) {
			enemySwarm.remove(enemy);
			enemy.kill();
			dragon.hit();
			
		}
	}
	
	private function spawnEnemies(number:Int) {
		for (i in 0...number) {
			var enemy:Enemy = new Enemy();
			enemy.x = Math.floor(Math.random() * (FlxG.width - enemy.width));
			enemy.y = Math.floor(Math.random() * -200 );
			enemySwarm.push(enemy);
			add(enemy);
		}
		
	}
}