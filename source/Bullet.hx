package ;
import flixel.FlxSprite;
import openfl.Assets;
import flixel.FlxG;

/**
 * ...
 * @author KodeIn
 */
class Bullet extends FlxSprite
{
	private var playState:PlayState;
	var enemies:Array<Enemy>;
	
	public function new(x:Float, y:Float, playstate:PlayState) {
		super();
		loadGraphic(AssetPaths.bullet__png);
		this.x = x - this.width/2;
		this.y = y;
		this.playState = playstate;
	}
	
	override public function update() {
		super.update();
		
		if (this.y < 0) {
			this.kill();
		}
		this.y -= 10;
		enemies = playState.getEnemies();
		for (i in 0...enemies.length) {
			FlxG.overlap(this, enemies[i], onHit);
		}
		
		
	}
	
	private function onHit(me:Bullet, enemy:Enemy) {
		this.kill();
		enemy.kill();
		enemies.remove(enemy);
	}
	
}