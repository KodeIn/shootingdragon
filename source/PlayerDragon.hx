package ;
import flixel.FlxSprite;
import openfl.Assets;
import flixel.FlxG;

/**
 * ...
 * @author KodeIn
 */
class PlayerDragon extends FlxSprite
{
	public static var DRAGON_SPEED = 6;
	
	private var life:Int;
	private var shootingCooldown:Int;
	private var playState:PlayState;
	
	public function new(playState:PlayState) {
		super();
		loadGraphic( AssetPaths.dragon__png);
		life = 10;
		shootingCooldown = 0;
		this.playState = playState;
	}
	
	public function hit() {
		life--;
		if (life <= 0) {
			FlxG.switchState(new MenuState());
		}
	}
	
	private function shoot() {
		var bullet = new Bullet(this.x + this.width/2, this.y, playState);
		playState.add(bullet);
	}
	
	override function update() {
		super.update();
		
		if (shootingCooldown > 0) {
			shootingCooldown--;
		}
		
		if (FlxG.keys.anyPressed(["DOWN"])) {
			this.y += DRAGON_SPEED;
		}
		if (FlxG.keys.anyPressed(["UP"])) {
			this.y -= DRAGON_SPEED;
		}
		if (FlxG.keys.anyPressed(["RIGHT"])) {
			this.x += DRAGON_SPEED;
		}
		if (FlxG.keys.anyPressed(["LEFT"])) {
			this.x -= DRAGON_SPEED;
		}
		
		if (FlxG.keys.anyPressed(["SPACE"]) && shootingCooldown <= 0) {
			shoot();
			shootingCooldown = 12;
		}
		
		if (this.y < 0) {
			this.y = 0;
		}
		if (this.y > FlxG.height - this.height) {
			this.y = FlxG.height - this.height;
		}
		
		if (this.x < 0) {
			this.x = 0;
		}
		if (this.x > FlxG.width - this.width) {
			this.x = FlxG.width - this.width;
		}
	}
}