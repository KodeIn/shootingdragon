package ;
import flixel.FlxSprite;
import openfl.Assets;
import flixel.FlxG;

/**
 * ...
 * @author KodeIn
 */
class Enemy extends FlxSprite
{
	private static var SPEED = 2;
	
	public function new() {
		super();
		
		loadGraphic(AssetPaths.enemy__png);
	}
	
	override public function update() {
		super.update();
		this.y += SPEED;
		
		if (this.y > (FlxG.height + 100)) {
			this.y = Math.floor(Math.random() * -200 ); 
			this.x = Math.floor(Math.random() * (FlxG.width - this.width));
		}
	}
}