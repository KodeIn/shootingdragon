# Projet ShootingDragon #

**Développement itératif** d'un shmup en live sur hitbox.

J'invite tout le monde à faire sa branche et bidouiller le code jusqu'à ce qu'on ne le reconnaisse plus.

## Pitch du jeu ##
C'est un dragon à qui on a piqué son nid (vous savez, les tonnes d'or et de joyaux).
Donc il va récupérer son bien en carbonisant ses voleurs.

## Participatif ##
Je voudrais que les spectateurs du stream participent activement au projet.
Par exemple :

* Faire des concours de création de sprites, bruitages, etc.
* Récolter des idées et tester les plus intéressantes/les plus faisables
* Tester différentes branches et voir ce qui fonctionne le mieux selon les spectateurs via un vote.
* etc.

## Développement itératif? ##
Cette méthode de travail me convient le mieux. Elle permet d'avoir un "truc à montrer" rapidement, ce qui me maintient motivé :p

En très résumé et très simplifié, c'est un cycle continu de prototype -> test -> analyse des résultats -> amélioration et là, on boucle sur la phase "test".
Si vous voulez en savoir plus, je vous invite à lire les pages wikipedia (en anglais uniquement) [iterative design](http://en.wikipedia.org/wiki/Iterative_design) et [Iterative and incremental developement](http://en.wikipedia.org/wiki/Iterative_and_incremental_development).
(me semble que ça correspond à peu près à la méthode AGILE)

## Technologies / Outils ##
* Langage :				[Haxe](http://haxe.org/)
* Framework :			[HaxeFlixel](http://haxeflixel.com/)
* IDE :					[FlashDevelop](http://www.flashdevelop.org/)
* Versionning :			[SourceTree](http://www.sourcetreeapp.com/) & [BitBucket's Git Repo](https://bitbucket.org/KodeIn/shootingdragon)
* Dessin vectoriel :	[Inkscape](https://inkscape.org/fr/)
* Dessin bitmap :		[Krita](https://krita.org/)
* Audio sfx :			[Bfxr](http://www.bfxr.net/)
* Audio musique :		???

Comme vous pouvez le remarquer, j'utilise un maximum d'outils libres/gratuits pour que tout le monde puisse participer!

## Licences ##
### Code Source ###
On va faire simple:
The MIT License (MIT)

Copyright (c) 2014 Jérémie Fery

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


### Concernant les fichiers sprites, backgrounds, musiques, sfx, polices de caractères, etc.###
Ne sachant pas trop quelle licence choisir vu la nature de mon projet, j'ai choisi la suivante :

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)
Cette œuvre est mise à disposition selon les termes de la [Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/).

Si les participants souhaitent utiliser une autre licence pour le contenu qu'ils proposent, j'ajouterai les informations.
Je tiens à ce que tous les auteurs de contenu soient cités et il le seront ci-dessous!
####Police####
VCR OSD Mono - mrmanet

####Sprites####
placeholder très moche : dragon, ennemi, boule de feu - Moi